#!/bin/bash
set -ex
#set -e
# -------------------------------------------------------------------------------
# Filename:     build-ffmpeg-mlu.sh
# UpdateDate:   2022/02/08
# Description:  Build ffmpeg-mlu.
# Example:      ./build-ffmpeg-mlu.sh
# Depends:
#               Driver(ftp://username@download.cambricon.com:8821/product/GJD/MLU270/1.7.604/Ubuntu16.04/Driver/neuware-mlu270-driver-dkms_4.9.8_all.deb)
#               CNToolkit(ftp://username@download.cambricon.com:8821/product/GJD/MLU270/1.7.604/Ubuntu16.04/CNToolkit/cntoolkit_1.7.5-1.ubuntu16.04_amd64.deb)
#               CNCV(ftp://username@download.cambricon.com:8821/product/GJD/MLU270/1.7.604/Ubuntu16.04/CNCV/cncv_0.4.602-1.ubuntu16.04_amd64.deb)
#               FFmpeg-MLU(https://github.com/Cambricon/ffmpeg-mlu)
#               FFmpeg(https://gitee.com/mirrors/ffmpeg.git -b release/4.2 --depth=1)
# Notes:
# -------------------------------------------------------------------------------
#Font color
none="\033[0m"
green="\033[0;32m"
red="\033[0;31m"
yellow="\033[1;33m"
white="\033[1;37m"
#ENV
PATH_WORK="ffmpeg-mlu"
WORK_DIR="/root/ffmpeg-mlu"
FILENAME_FFMPEG_MLU="ffmpeg-mlu-v4.1.2.tar.gz"

#############################################################
# 1. Compile and install FFmpeg-MLU
## 1.1. Download FFmpeg-MLU
cd $WORK_DIR
PATH_WORK_TMP="ffmpeg-mlu"
#Github clone 指定分支及commit的代码,避免版本更新迭代造成不兼容.
#GITHUB_URL="https://github.com/Cambricon/ffmpeg-mlu.git"
GITHUB_URL="https://gitee.com/cambricon/ffmpeg-mlu.git"
GITHUB_Branch="master"
# for CNToolkit3.14.0 + CNCV2.7.4 + FFMPEG4.1.2
GITHUB_Commit="68022d1b2e20f408dfed9ad5014c7e9dc07e20f8"
# for CNToolkit3.14.0 + CNCV2.7.4 + FFMPEG4.1.1
#GITHUB_Commit="3951e69ba328776f19697eb5dff4ca84c7dee89c"
# for SDK1.19.0/1.18.0 + ffmpeg 3.5.0
#GITHUB_Commit="029ddc5f1097827c243a436f93adf4f7846872f2"
# for 1.17.0 + ffmpeg 3.5.0
#GITHUB_Commit="029ddc5f1097827c243a436f93adf4f7846872f2"
# for 1.17.0
#GITHUB_Commit="5e828ab7beafb6a1787cb9198152432d9342119d"
# for 1.15.0
#GITHUB_Commit="f5d6d5ca3bfaeb3f338cb76b6c45de461709b4f0"
# for 1.14.0
#GITHUB_Commit="f5d6d5ca3bfaeb3f338cb76b6c45de461709b4f0"
#GITHUB_Commit="46f127071e697833f2147b5fd2149c6e6ed4e1b6"
#GITHUB_Commit="aa5e6ebc0afbde70f918ef79aa3b00d888db7864"

if [ -f "${FILENAME_FFMPEG_MLU}" ];then
    echo -e "${green}File(${FILENAME_FFMPEG_MLU}): Exists!${none}"
    # $FILENAME_FFMPEG_MLU 压缩包中已经包含了ffmpeg-mlu补丁 + ffmpeg4.2
    tar zxvf $FILENAME_FFMPEG_MLU
else
    echo -e "${red}File(${FILENAME_FFMPEG_MLU}): Not exist!${none}"
    echo -e "${yellow}1.Please download ${FILENAME_FFMPEG_MLU} from FTP(ftp://download.cambricon.com:8821/***)!${none}"
    echo -e "${yellow}  For further information, please contact us.${none}"
    echo -e "${yellow}2.Copy the dependent packages(${FILENAME_FFMPEG_MLU}) into the directory!${none}"
    echo -e "${yellow}  eg:cp -v ./dependent_files/${FILENAME_FFMPEG_MLU} ./${PATH_WORK}${none}"
    echo -e "${green}3.Downloading automatically......${none}"

    if [ ! -d "${PATH_WORK_TMP}" ];then
        git clone ${GITHUB_URL} -b ${GITHUB_Branch}
        cd ${PATH_WORK_TMP} && git checkout ${GITHUB_Commit} && cd -
    else
        echo "Directory($PATH_WORK_TMP): Exists!"
    fi
fi

## 1.4. 设置环境变量
export NEUWARE_HOME=/usr/local/neuware
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${NEUWARE_HOME}/lib64

## 1.5. 编译FFmpeg-MLU
pushd $PATH_WORK_TMP
#删除--prefix选项是配置安装的路径,将FFmpeg—MLU直接安装到系统目录下
#sed -i '/--prefix=/d' ./compile_ffmpeg.sh
cp -rvf /home/share/tools/compile_ffmpeg.sh ./compile_ffmpeg.sh
#基于FFmpeg-MLU自带的编译脚本，修改了安装路径为默认路径。默认编译版本是4.4
#./compile_ffmpeg.sh 4.2
./compile_ffmpeg.sh
popd

cd $WORK_DIR
#############################################################
# 2.Test FFMpeg-MLU
#./test-ffmpeg-mlu.sh