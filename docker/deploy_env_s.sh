#!/bin/bash
set -ex
#set -e
# -------------------------------------------------------------------------------
# Filename:     deploy_env_s.sh
# UpdateDate:   2024/10/10
# Description:  一键搭建验证环境。
# Example:      cd /home/share/docker && ./deploy_env_s.sh
# Depends:
# Notes:        本脚本适用于初次进入docker容器环境；不适用于再次进入容器，可能会有意想不到问题
# -------------------------------------------------------------------------------

## 一键搭建验证环境
cp -rvf /home/share/docker/pre_packages22.04.sh /root/ffmpeg-mlu \
    && cd /root/ffmpeg-mlu \
    && ./pre_packages22.04.sh \
    && cp -rvf /home/share/docker/build-ffmpeg-mlu.sh /root/ffmpeg-mlu \
    && cd /root/ffmpeg-mlu \
    && ./build-ffmpeg-mlu.sh
