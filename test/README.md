# 基于ffmpeg-mlu的性能测试

```bash
.
├── api                                 #API方式测试
│   ├── clean.sh
│   ├── test-ffmpeg-mlu-api.sh
│   └── transcode
├── cmd                                 #命令方式测试
│   ├── test-ffmpeg-mlu-cmd-decode.sh   #命令方式测试370解码性能
│   ├── test-ffmpeg-mlu-cmd-encode.sh   #命令方式测试370编码性能
│   └── test-ffmpeg-mlu-cmd.sh
├── data                                #测试数据
│   └── test.mp4
└── README.md                           #README
```

# 基于cncodec的性能测试
```bash
if [ -e /usr/local/bin/jpeg_dec_perf ]; then exit 0; fi
# 基于 cncodec 的性能测试
#编译测试代码
cd /usr/local/neuware/samples/cncodec_v3
if [ ! -d build ]; then mkdir -p build; fi
cd build && cmake ../ && make
#拷贝可执行程序到系统用户bin目录
cp -rvf /usr/local/neuware/samples/cncodec_v3/bin/* /usr/local/bin
#测试图片解码(jpeg_dec_perf使用方法可查看帮助信息)
jpeg_dec_perf -i /home/share/test/data/images/a.jpg -thread 10 -mlu 0 -l 1000 -monitor
#参考以下命令查看硬件资源占用情况
#while true;do echo "==========================================";cnmon info -c 0 | grep -E "Image Codec|Board|Device CPU Chip|DDR|Chip|Memory|Used|Usage|Power";sleep 0.5;done

#1.jpeg 编码性能测试
#1080P
jpeg_enc_perf -i /home/share/test/data/1920x1080_nv12.yuv -thread 128 -s 1920x1080 -if NV12 -l 1000 -mlu 0 -pq 85 -host
#4K
jpeg_enc_perf -i /home/share/test/data/3840x2160_nv12.yuv -thread 128 -s 1920x1080 -if NV12 -l 1000 -mlu 0 -pq 85 -host


#2.jpeg 解码性能测试
#1080p
jpeg_dec_perf -i /home/share/test/data/1920x1080.jpg -thread 128 -l 1000 -mlu 0
#4K
jpeg_dec_perf -i /home/share/test/data/3840x2160.jpg -thread 32 -l 1000 -mlu 0

#3.视频解码测试
#解码序
#video_dec_perf -i /home/share/test/data/jellyfish-3-mbps-hd-h264.mkv -thread 10 -fps 30 -mlu 0 -monitor -decode_order
#显示序
#video_dec_perf -i /home/share/test/data/jellyfish-3-mbps-hd-h264.mkv -thread 10 -fps 30 -mlu 0 -monitor
#h264
video_dec_perf -i /home/share/test/data/jellyfish-10-mbps-hd-h264.mkv -thread 132 -mlu 0
#h265
video_dec_perf -i /home/share/test/data/jellyfish-10-mbps-hd-hevc.mkv -thread 132 -mlu 0

#4.视频编码测试
#h264
video_enc_perf -i /home/share/test/data/jellyfish-900-1920x1080_nv12.yuv -s 1920x1080 -if NV12 -codec H264 -frame 900 -thread 24 -mlu 0 -rc vbr -bitrate 5M
#h265
video_enc_perf -i /home/share/test/data/jellyfish-900-1920x1080_nv12.yuv -s 1920x1080 -if NV12 -codec HEVC -frame 900 -thread 24 -mlu 0 -rc vbr -bitrate 5M
```