#!/bin/bash
set -ex
#set -e
USERNAME=$(whoami)
CMD_TIME=$(date +%Y%m%d%H%M%S) # eg:20210402230402
# 0.Check param
if [[ $# -eq 1 ]];then CMD_TIME="${1}";fi

cd ../
# 1.Source env
source "./env.sh"
# 2.gzip

ls -la image-${OS}-${PATH_WORK}-${VERSION}-${CMD_TIME}.tar.gz* \
    && sudo chown $USERNAME:$USERNAME image-${OS}-${PATH_WORK}-${VERSION}-${CMD_TIME}.tar.gz \
    && mv image-${OS}-${PATH_WORK}-${VERSION}-${CMD_TIME}.tar.gz image-${OS}-${PATH_WORK}-${VERSION}-${CMD_TIME}.tar \
    && gzip image-${OS}-${PATH_WORK}-${VERSION}-${CMD_TIME}.tar \
    && md5sum image-${OS}-${PATH_WORK}-${VERSION}-${CMD_TIME}.tar.gz > image-${OS}-${PATH_WORK}-${VERSION}-${CMD_TIME}.tar.gz.md5sum \
    && ls -lh image-${OS}-${PATH_WORK}-${VERSION}-${CMD_TIME}.tar.gz*
cd -
