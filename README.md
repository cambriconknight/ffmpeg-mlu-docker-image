<p align="center">
    <a href="https://gitee.com/cambriconknight/ffmpeg-mlu-docker-image">
        <h1 align="center">FFmpeg-MLU环境搭建与验证</h1>
    </a>
</p>

**该教程仅仅用于学习，打通流程； 不对效果负责，不承诺商用。**

# 1. 概述

本工具集主要基于Docker容器进行[FFmpeg-MLU](https://github.com/Cambricon/ffmpeg-mlu)环境搭建与验证。力求压缩寒武纪FFmpeg-MLU环境搭建与功能验证的时间成本, 以便快速上手寒武纪FFmpeg-MLU。

<p align="left">
    <img alt="ffmpeg-mlu" src="./res/ffmpeg-mlu-docker-image-1.png" height="360" />
</p>

**说明:**

基于寒武纪® MLU硬件平台，寒武纪 FFmpeg-MLU使用纯C接口实现硬件加速的视频编解码。

## 1.1. 硬件环境准备

| 名称            | 数量       | 备注                |
| :-------------- | :--------- | :------------------ |
| 开发主机/服务器 | 一台       |主流配置即可；电源功率按需配置；PCIe Gen.3 x16/Gen.4 x16 |
| MLU370        | 一套       | 二选一, 尽量避免混插混用 |

## 1.2. 软件环境准备

| 名称                   | 版本/文件                                    | 备注            |
| :-------------------- | :-------------------------------             | :--------------- |
| Linux OS              | Ubuntu22.04/Ubuntu20.04/CentOS7   | 驱动以宿主机操作系统选择   |
| Driver_MLU370         | cambricon-mlu-driver-ubuntu22.04-dkms_6.2.2-5_amd64.deb    | 下载地址请联系厂家技术支持   |
| CNToolkit_MLU370      | cntoolkit_3.14.0-1.ubuntu22.04_amd64.deb   | 下载地址请联系厂家技术支持   |
| CNCV_MLU370           | cncv_2.7.4-1.ubuntu22.04_amd64.deb    | 下载地址请联系厂家技术支持    |
| FFmpeg-MLU            | FFmpeg-MLU   | commit： 68022d； 脚本会自动[下载](https://gitee.com/cambricon/ffmpeg-mlu)    |
| FFmpeg                | FFmpeg       | branch： 4.4； 脚本会自动[下载](https://gitee.com/mirrors/ffmpeg.git)    |

*以上软件包涉及FTP手动下载的,可下载到本地[dependent_files](./dependent_files)目录下,方便对应以下步骤中的提示操作。*

*以上软件包也可关注微信公众号 【 AIKnight 】, 发送关键字 **FFmpeg-MLU** 自动获取,获取后放置到本地[dependent_files](./dependent_files)目录下,方便对应以下步骤中的提示操作。*

**AIKnight公众号**
>![](./res/aiknight_wechat_344.jpg)

## 1.3. 资料下载

MLU开发文档: https://developer.cambricon.com/index/document/index/classid/3.html

Neuware SDK: https://cair.cambricon.com/#/home/catalog?type=SDK%20Release

其他开发资料, 可前往[寒武纪开发者社区](https://developer.cambricon.com)注册账号按需下载。也可在官方提供的专属FTP账户指定路径下载。

# 2. 目录结构

*当前仓库默认基于Docker 进行FFmpeg-MLU 环境搭建与验证。按照以下章节步骤即可快速实现FFmpeg-MLU环境搭建与验证*

```bash
.
├── build-image-ffmpeg-mlu.sh   #此脚本用于编译Docker 镜像用于搭建FFmpeg-MLU 环境
├── clean.sh                    #清理Build出来的临时目录或文件,包括镜像文件,已加载的镜像,已加载的容器等
├── dependent_files             #此目录主要用于存储仓库所需依赖文件
│   └── README.md
├── docker                      #此目录主要用于存储编译Docker 镜像及验证FFmpeg-MLU 所需依赖文件
│   ├── build-ffmpeg-mlu.sh     #此脚本用于编译FFmpeg-MLU 及相关依赖项, 也可用于裸机下环境搭建
│   ├── clean.sh                #清理当前目录下新编译生存的Docker 镜像文件
│   ├── deploy_env_s.sh         #一键搭建验证环境脚本
│   ├── Dockerfile.16.04        #用于编译Docker 镜像的Dockerfile 文件
│   ├── Dockerfile.18.04        #用于编译Docker 镜像的Dockerfile 文件
│   ├── Dockerfile.20.04        #用于编译Docker 镜像的Dockerfile 文件
│   ├── Dockerfile.22.04        #用于编译Docker 镜像的Dockerfile 文件
│   ├── install_cntoolkit.sh    #安装cntoolkit脚本
│   ├── pip.conf                #切换python的pip源
│   ├── pre_packages18.04.sh    #安装基于操作系统所需依赖包, 也可用于裸机下环境搭建
│   ├── pre_packages20.04.sh    #安装基于操作系统所需依赖包, 也可用于裸机下环境搭建
│   ├── pre_packages22.04.sh    #安装基于操作系统所需依赖包, 也可用于裸机下环境搭建
│   ├── pre_packages.sh         #安装基于操作系统所需依赖包, 也可用于裸机下环境搭建
│   ├── requirements.txt        #requirements文件
│   ├── sources_16.04.list      #Ubuntu16.04 sources文件
│   ├── sources_18.04.list      #Ubuntu18.04 sources文件
│   ├── sources_20.04.list      #Ubuntu20.04 sources文件
│   └── sources_22.04.list      #Ubuntu22s.04 sources文件
├── load-image-ffmpeg-mlu.sh    #加载Docker 镜像
├── README.md                   #README
├── res                         #资源图库
├── run-container-ffmpeg-mlu.sh #启动Docker 容器
├── save-image-ffmpeg-mlu.sh    #导出镜像文件，实现镜像内容持久化
├── sync.sh                     #同步[dependent_files] 到临时目录[ffmpeg-mlu]
├── test                        #测试FFmpeg-MLU 相关功能目录
│   ├── api                     #API方式测试
│   ├── cmd                     #命令方式测试
│   ├── data                    #测试数据
│   └── README.md
└── tools                       #常用工具存放目录
```

*如需在裸机HOST上进行环境搭建, 也可以利用[docker](./docker)目录以下脚本实现快速搭建。*

```bash
.
├── docker
│   ├── build-ffmpeg-mlu.sh     #此脚本用于编译FFmpeg-MLU 及相关依赖项, 也可用于裸机下环境搭建
│   ├── pre_packages22.04.sh    #安装基于操作系统所需依赖包, 也可用于裸机下环境搭建
│   └── sources_22.04.list      #Ubuntu22.04 sources文件
```

# 3. 代码下载
```bash
#git clone https://github.com/CambriconKnight/ffmpeg-mlu-docker-image.git
git clone https://gitee.com/cambriconknight/ffmpeg-mlu-docker-image.git
```

# 4. 编译镜像
```bash
#编译 ffmpeg-mlu 镜像
./build-image-ffmpeg-mlu.sh
```
**编译镜像实例**
<p align="left">
    <img alt="ffmpeg-mlu_build_100ms" src="https://gitee.com/cambriconknight/dev-open-res/raw/main/ffmpeg-mlu-docker-image/res/ffmpeg-mlu_build_100ms.gif" width="640" />
</p>

# 5. 加载镜像
```bash
#加载Docker镜像
sudo ./load-image-ffmpeg-mlu.sh ./docker/image-ubuntu22.04-ffmpeg-mlu-v1.20.1.tar.gz
```
**加载镜像实例**
```bash
[username@worker1~/nfs/gitee/ffmpeg-mlu-docker-image]$sudo ./load-image-ffmpeg-mlu.sh ./docker/image-ubuntu22.04-ffmpeg-mlu-v1.20.1.tar.gz
0
kang/ubuntu22.04-ffmpeg-mlu:v1.20.1
The image(kang/ubuntu22.04-ffmpeg-mlu:v1.20.1) is not loaded and is loading......
Loaded image: kang/ubuntu22.04-ffmpeg-mlu:v1.20.1
The image(kang/ubuntu22.04-ffmpeg-mlu:v1.20.1) information:
REPOSITORY                    TAG       IMAGE ID       CREATED              SIZE
kang/ubuntu22.04-ffmpeg-mlu   v1.20.1   fbe86279c74b   About a minute ago   1.42GB
[username@worker1~/nfs/gitee/ffmpeg-mlu-docker-image]$
```

# 6. 启动容器
```bash
#启动Docker容器
sudo ./run-container-ffmpeg-mlu.sh
# 安装第三方库，根据实际网络环境情况，需要一定安装时间
cp -rvf /home/share/docker/pre_packages22.04.sh /root/ffmpeg-mlu
cd /root/ffmpeg-mlu
./pre_packages22.04.sh
# 编译ffmpeg-mlu
cp -rvf /home/share/docker/build-ffmpeg-mlu.sh /root/ffmpeg-mlu
cd /root/ffmpeg-mlu
./build-ffmpeg-mlu.sh
```
**编译FFMpeg-MLU实例**
<p align="left">
    <img alt="build-ffmpeg-mlu" src="https://gitee.com/cambriconknight/dev-open-res/raw/main/ffmpeg-mlu-docker-image/res/build-ffmpeg-mlu.gif" width="640" />
</p>

**实例**
```bash
[ username@worker1~/nfs/gitee/ffmpeg-mlu-docker-image]$sudo ./run-container-ffmpeg-mlu.sh
0
container-ubuntu22.04-ffmpeg-mlu-v1.20.1-kang
WARNING: Published ports are discarded when using host network mode
root@worker1:/home/share# cp -rvf /home/share/docker/pre_packages22.04.sh /root/ffmpeg-mlu
'/home/share/docker/pre_packages22.04.sh' -> '/root/ffmpeg-mlu/pre_packages22.04.sh'
root@worker1:/home/share# cd /root/ffmpeg-mlu
root@worker1:~/ffmpeg-mlu# ./pre_packages22.04.sh
```

# 7. 测试验证

*以下测试验证步骤都是基于Docker容器内环境.*

## 7.1. MLU解码
```bash
#基于 FFMPEG 命令行方式验证多路并行解码, 可用于上手阶段压测MLU板卡硬件解码能力.
cd /home/share/test/cmd
./test-ffmpeg-mlu-cmd-decode.sh
#此plus脚本在运行核心业务过程中可以实时显示显示业务日志文件并记录到日志文件，并且还会打印cnmon相关信息到日志文件。
#./test-ffmpeg-mlu-cmd-decode-plus.sh ../data/jellyfish-3-mbps-hd-h264.mkv 0 128 h264_mludec
```
**MLU解码实例-解码128路高清视频**
<p align="left">
    <img alt="test-ffmpeg-mlu-cmd-decode" src="https://gitee.com/cambriconknight/dev-open-res/raw/main/ffmpeg-mlu-docker-image/res/test-ffmpeg-mlu-cmd-decode.gif" width="640" />
</p>

## 7.2. MLU编码
```bash
#基于 FFMPEG 命令行方式验证多路并行编码, 可用于上手阶段压测MLU板卡硬件编码能力.
cd /home/share/test/cmd
./test-ffmpeg-mlu-cmd-encode.sh
```
**MLU编码实例-编码24路高清视频**
<p align="left">
    <img alt="test-ffmpeg-mlu-cmd-encode" src="https://gitee.com/cambriconknight/dev-open-res/raw/main/ffmpeg-mlu-docker-image/res/test-ffmpeg-mlu-cmd-encode.gif" width="640" />
</p>

## 7.3. MLU转码
```bash
#基于FFMPEG转码有两种方式
#1、命令行方式
cd /home/share/test/cmd
./test-ffmpeg-mlu-cmd.sh
#基于 FFMPEG 命令行方式验证多路并行转码, 可用于上手阶段压测MLU板卡硬件编解码能力.
#./test-ffmpeg-mlu-cmd-transcode.sh
#此plus脚本在运行核心业务过程中可以实时显示显示业务日志文件并记录到日志文件，并且还会打印cnmon相关信息到日志文件。
#./test-ffmpeg-mlu-cmd-transcode-plus.sh ../data/jellyfish-3-mbps-hd-h264.mkv 0 24 h264_mludec h264_mluenc "1920x1080"
#./test-ffmpeg-mlu-cmd-transcode-plus.sh ../data/jellyfish-3-mbps-hd-h264.mkv 0 50 h264_mludec h264_mluenc "352x288"
#2、API接口调用方式
cd /home/share/test/api
./test-ffmpeg-mlu-api.sh
```

# 8. 保存镜像
保存镜像，供后续环境变更直接使用。
```bash
./save-image-ffmpeg-mlu.sh
```
**保存镜像实例**
```bash
[username@worker1~/]$sudo ./save-image-ffmpeg-mlu.sh test
[sudo] password for username:
[# Docker images:
kang/ubuntu22.04-ffmpeg-mlu     v1.20.1     647453e6443e    9 minutes ago   1.42GB
   Images does not exist!
[# Docker container:
773788382bd7    kang/ubuntu22.04-ffmpeg-mlu:v1.20.1  "/bin/bash"    7 minutes ago   Up 7 minutes    container-ubuntu22.04-ffmpeg-mlu-v1.20.1-kang
[# Commit docker container:
sha256:6f9418a475b7a1b83f4d8e283a061ca303811e40a7b0e60b4e678843b0bd56ea
   Completed!
[# Docker images:
kang/ubuntu22.04-ffmpeg-mlu    v1.20.1-test 6f9418a475b7    1 second ago    3.94GB
kang/ubuntu22.04-ffmpeg-mlu     v1.20.1     647453e6443e   10 minutes ago   1.42GB
[# Save docker image:
-rw------- 1 root root 3.8G 7月  16 11:04 image-ubuntu22.04-ffmpeg-mlu-v1.20.1-test.tar.gz
   Completed!
```

**压缩镜像实例**
如果感觉镜像文件太大，不方便后期传输。可参考以下步骤，对镜像文件进行压缩并记录md5值：
```bash
cd /data/gitee/ffmpeg-mlu-docker-image/tools
bash ./gzip.sh test
```
